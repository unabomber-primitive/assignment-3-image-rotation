#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>
#include <stdlib.h>

struct __attribute__ ((packed)) pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image image_create(size_t width, size_t height);
struct pixel *get_pixel(const struct image* this, size_t col, size_t row);
void image_destroy(struct image *restrict img);
#endif
