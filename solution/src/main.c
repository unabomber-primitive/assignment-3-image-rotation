#include <stdio.h>

#include "../include/image.h"
#include "../include/io_bmp.h"
#include "../include/transformation_rotate.h"

int main(int argc, char** argv) {
    if (argc < 3) {
        fprintf(stderr, "как надо: %s <source-image> <transformed-image>\n", argv[0]);
        return -1;
    }

    char *input_filename = argv[1];
    char *out_filename = argv[2];

    FILE* in = fopen(input_filename, "rb");

    if (!in) {
	    return -1;
    }


    struct image img = (struct image) {0};

    switch (from_bmp(in, &img)) {
        case READ_OK:
            break;
        case READ_INVALID_HEADER:
            fprintf(stderr, "некорректный заголовок изображения '%s'\n", input_filename);
            return -1;
        case READ_INVALID_BITS:
            fprintf(stderr, "некорреткные данные из изображения '%s'\n", input_filename);
            return -1;
        case READ_ERROR:
            fprintf(stderr, "ошибка чтения");
            return -1;
        case READ_INVALID_SIGNATURE:
            fprintf(stderr, "ошибка");
            return -1;
    }

    if (fclose(in) == EOF) {
	    return -1;
    }

    struct image transformed = rotate(img);
    image_destroy(&img);

    if (transformed.data == NULL) {
	    printf("rotation error");
	    return -1;
    }

    FILE* out = fopen(out_filename, "w");

    if (!out) {
	    return -1;
    }


    switch (to_bmp(out, &transformed)) {
        case WRITE_OK:
            break;
        case WRITE_ERROR:
            fprintf(stderr, "не сохранилось :((( в файл '%s'\n", out_filename);
            return -1;
    }
    image_destroy(&transformed);

    if (fclose(out) == EOF) {
	    return -1;
    }


    return 0;
}
