#include "../include/transformation_rotate.h"

struct image rotate(const struct image src) {
    struct image target = image_create(src.height, src.width);
    for (size_t y = 0; y < src.height; ++y) {
        for (size_t x = 0; x < src.width; ++x) {
         //target.data[y * target.width + x] = src.data[(src.height - 1 - x) * src.width + y];
         *get_pixel(&target, src.height - y - 1, x) = *get_pixel(&src, x, y);
        }
    }
    return target;
}
