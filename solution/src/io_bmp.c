#include "../include/io_bmp.h"

enum {
	BF_TYPE = 19778,
	FILE_HEADER_SIZE = 14,
	BI_PLANES = 1,
	BI_BIT_COUNT = 24,
	HEADER_SIZE = sizeof(struct bmp_header),
	ZERO_VAL = 0,
	PIXEL_SIZE = sizeof(struct pixel)
};

static uint8_t padding(const size_t width) {
    return (4 - (sizeof(struct pixel) * width)) % 4;
}

static struct bmp_header create_header(struct image image) {

	size_t offset = HEADER_SIZE;
	size_t inf_size = offset - FILE_HEADER_SIZE;
	//size_t row_size = PIXEL_SIZE * image.width;
	//size_t padded_row_size = row_size + padding(row_size);
	//size_t image_size = padded_row_size * image.height;


    return (struct bmp_header) {
            .bfType = BF_TYPE,
            .bfileSize = sizeof(struct bmp_header) +
                         image.height * (sizeof(struct pixel) * image.width +
                                         padding(image.width)),
            .bfReserved = ZERO_VAL,
            .bOffBits = offset,
            .biSize = inf_size,
            .biWidth = image.width,
            .biHeight = image.height,
            .biPlanes = BI_PLANES,
            .biBitCount = BI_BIT_COUNT,
            .biCompression = ZERO_VAL,
            .biSizeImage = image.height * (sizeof(struct pixel) * image.width +
                                           padding(image.width)),
            .biXPelsPerMeter = ZERO_VAL,
            .biYPelsPerMeter = ZERO_VAL,
            .biClrUsed = ZERO_VAL,
            .biClrImportant = ZERO_VAL
    };
}

//enum read_status from_bmp(FILE* in, struct image* img);
enum read_status from_bmp(FILE* in, struct image* img ) {
    struct bmp_header header = {0};

    if (!fread(&header, HEADER_SIZE, 1, in)) {
        return READ_INVALID_HEADER;
    }


    if (header.bfType != 0x4d42 && header.bfType != 0x4349 && header.bfType != 0x5450 ) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != 24 ) {
        return READ_INVALID_BITS;
    }

    *img = image_create(header.biWidth, header.biHeight);

    if (img->data == NULL) {
		return READ_ERROR;
    }

    for ( size_t row = 0; row < img->height; ++row ) {
        size_t count_read =  fread( img->data + img->width * row, PIXEL_SIZE, img->width, in);
        if ( count_read != img->width ) {
            return READ_ERROR;
        }
        if ( fseek( in, padding(img->width), SEEK_CUR ) ) {
            return READ_ERROR;
        }
    }

    return READ_OK;

}


//enum write_status to_bmp(FILE* out, struct image const* img);

enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header = create_header(*img);

    if (!fwrite(&header, HEADER_SIZE, 1, out)) {
        return WRITE_ERROR;
    }

    for (size_t row = 0; row < img->height; ++row) {
        if (!fwrite(img->data + row * img->width, PIXEL_SIZE * img->width, 1, out)) {
            return WRITE_ERROR;
        }

		if (fseek(out, padding(img->width), SEEK_CUR) == -1) {
			return WRITE_ERROR;
		}
    }
    return WRITE_OK;
}
