#include "../include/image.h"

struct image image_create(size_t width, size_t height) {
	struct pixel* data = malloc( width * height * sizeof(struct pixel));
    return (struct image) {.width = width, .height = height, .data = data};
}

void image_destroy(struct image *restrict img) {
	img->height = 0;
	img->width = 0;
	free(img->data);
	img->data = NULL;
}


struct pixel *get_pixel(const struct image *const this, size_t col, size_t row) {
	return this->data + col + row * this->width;
}
